var parser = require('luaparse');
const express = require('express')
var bodyParser = require('body-parser')
const axios = require('axios')
const fs = require('fs')
const path = require('path')
var cors = require('cors')

const app = express()
const port = 3235
const filname = 'warehouses';
app.use(bodyParser.json({ type: 'application/*+json' }))
app.get('/', async (req, res) => {
    //   res.send('Hello World!')
    var luaSrc = await fs.readFileSync(`./${filname}`, 'utf8')
    var ast = await parser.parse(luaSrc);
    console.log(JSON.stringify(ast));
    res.send(ast)
});
app.get('/parsed', async (req, res) => {
    var resp = await start();
    res.send(resp)
});
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})


async function start() {
    var missionJson = await fs.readFileSync(`./${filname}.json`, 'utf8')

    // var ast = parser.parse(luaSrc);
    // console.log(JSON.stringify(ast));
    // console.log((missionJson));


    missionJson = JSON.parse(missionJson);
    var fields = missionJson.body[0].init[0].fields;

    var resultJson = {};
    await parse(fields, resultJson)

    console.log(resultJson);
    return resultJson

}


function parse(fields, resultJson) {
    if (fields.length) {

        for (let i = 0; i < fields.length; i++) {
            const element = fields[i];
            const keys = element.key.raw.replace(/\"/g, "");
            console.log(keys);
            resultJson[keys] = {}
            if (element.value && element.value.fields) {
                parse(element.value.fields, resultJson[keys])
            }

        }

    }
}

// start();


