/// <reference path="node_modules/cesium/Source/Cesium.d.ts">

// const viewer = new Cesium.Viewer('cesiumContainer', {
//     terrainProvider: Cesium.createWorldTerrain()
// });
// const osmBuildings = viewer.scene.primitives.add(Cesium.createOsmBuildings());

// var layers = viewer.scene.imageryLayers;
// var blackMarble = layers.addImageryProvider();

var viewer = new Cesium.Viewer('cesiumContainer', {
    imageryProvider: new Cesium.TileMapServiceImageryProvider({
        url: Cesium.buildModuleUrl('Assets/Textures/NaturalEarthII')
    }),
    baseLayerPicker: false,
    geocoder: false,
    sceneModePicker: false,
});
// viewer.scene.primitives.add(Cesium.createOsmBuildings());
viewer._cesiumWidget._creditContainer.style.display = "none";

var scene = viewer.scene;

var tileset = scene.primitives.add(new Cesium.Cesium3DTileset({
    url: "http://localhost:3234/tileset.json",
    // url: Cesium.IonResource.fromAssetId(5743),
    // maximumScreenSpaceError: 2,
    // clippingPlanes: new Cesium.ClippingPlaneCollection({
    //     planes: clippingPlanes,
    //     edgeWidth: 1.0
    // })
}));

console.log(tileset);

// this.tileSet = new Cesium.Cesium3DTileset({
//     url: "http://localhost:3000/tileset.json",
// });


// var modelMatrix = Cesium.Transforms.eastNorthUpToFixedFrame(Cesium.Cartesian3.fromDegrees(73.0243, 26.2389,30));
// var model = scene.primitives.add(Cesium.Model.fromGltf({
//     url: './ts.glb',
//     modelMatrix: modelMatrix,
//     scale: 20000.0
// }));
// var scene = viewer.scene;
// var modelMatrix = Cesium.Transforms.eastNorthUpToFixedFrame(Cesium.Cartesian3.fromDegrees(73.0243, 26.2389,0));
// var model = scene.primitives.add(Cesium.Model.fromGltf({
//     url: './jd2.glb',
//     modelMatrix: modelMatrix,
//     scale: 20.0
// }));
var modelMatrix2 = Cesium.Transforms.eastNorthUpToFixedFrame(Cesium.Cartesian3.fromDegrees(70.9083, 26.9157,0));
var model2 = scene.primitives.add(Cesium.Model.fromGltf({
    url: './ny.glb',
    modelMatrix: modelMatrix2,
    // scale: 200.0
}));
// var modelMatrix3 = Cesium.Transforms.eastNorthUpToFixedFrame(Cesium.Cartesian3.fromDegrees(-71.057083, 42.361145 ,0));
// var model3 = scene.primitives.add(Cesium.Model.fromGltf({
//     url: './model.glb',
//     modelMatrix: modelMatrix3,
//     scale: 500.0
// }));
// var us_states = new Cesium.WebMapServiceImageryProvider({
//     // url: './map.osm',
//     url: 'http://localhost:8082/geoserver/wms',
//     parameters: {
//         format: 'image/png',
//         transparent: 'true',
//         tiled: true,
//         enablePickFeatures: true
//     },
//     layers: 'topp:states',  // comma separated listing
//     maximumLevel: 12
// });
// viewer.imageryLayers.addImageryProvider(us_states);

// const imageryLayers = viewer.imageryLayers;
// const provider = 
//     new Cesium.WebMapServiceImageryProvider({
//         url:
//             // 'http://localhost:8082/geoserver/topp/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=topp%3Astates&maxFeatures=50',
//             // 'http://localhost:8082/geoserver/topp/wms?service=WMS&version=1.1.0&request=GetMap&layers=topp%3Astates&bbox=-124.73142200000001%2C24.955967%2C-66.969849%2C49.371735&width=768&height=330&srs=EPSG%3A4326&styles=&format=image%2Fpng',
//             './topp-states.tif',
//             // "https://nationalmap.gov.au/proxy/http://geoserver.nationalmap.nicta.com.au/geotopo_250k/ows",
//         layers: "topp:states",
//         // parameters: {
//         //     // transparent: true,
//         //     // format: "image/png",
//         // },
//     })
// viewer.imageryLayers.addImageryProvider(provider);

// layers.addImageryProvider(new Cesium.SingleTileImageryProvider({
//     url: './land_shallow_topo_8192.tif',
//     rectangle: Cesium.Rectangle.fromDegrees(-75.0, 28.0, -67.0, 29.75)
// }));