/// <reference path='./babylon.d.ts'>

var canvas = document.getElementById("renderCanvas");

var startRenderLoop = function (engine, canvas) {
    engine.runRenderLoop(function () {
        if (sceneToRender && sceneToRender.activeCamera) {
            sceneToRender.render();
        }
    });
}

var engine = null;
var scene = null;
var sceneToRender = null;
var createDefaultEngine = function () { return new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true, disableWebGL2Support: false }); };
const createScene = async () => {
    const scene = new BABYLON.Scene(engine);
    // scene.useRightHandedSystem = true;
    const camera = new BABYLON.ArcRotateCamera("camera", -Math.PI / 2, 0, 5, new BABYLON.Vector3(0, 0, 0));
    camera.lowerBetaLimit = 0.4;
    // camera.upperBetaLimit = (Math.PI / 2) -0.01;
    camera.upperBetaLimit = (Math.PI / 2) * 0.9;
    camera.lowerRadiusLimit = 4;
    camera.upperRadiusLimit = 850;
    camera.attachControl(canvas, true);

    const light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 100, 0));

    // const box2 = BABYLON.MeshBuilder.CreateBox("box", { height: 1, width: 1, depth: 1 });

    const box = BABYLON.MeshBuilder.CreateBox("box", { size: 40 });
    // const box = BABYLON.MeshBuilder.CreateBox("box", { height: 0.3, width: 0.3, depth: 0.3 });
    // box.position.x = vector.x;
    
    var ground = BABYLON.MeshBuilder.CreateGround("ground", {width: 6, height: 6}, scene);
    var material = new BABYLON.StandardMaterial(scene);
    material.alpha = 1;
    material.diffuseColor = new BABYLON.Color3(1.0, 0.2, 0.7);
    box.material = material;


    // const nxuObj = await BABYLON.SceneLoader.AppendAsync(["mesh"],"./", "nxu.fbx", scene);

    // BABYLON.SceneLoader.Append("./", "FF2.obj", scene, function (scene) {
    //     // do something with the scene
    // });


    BABYLON.SceneLoader.Append("./", "LittlestTokyo.glb", scene, function (scene) {
        // do something with the scene
    });

    const axes = new BABYLON.AxesViewer(scene, 1)
    axes.xAxis.parent = box;
    axes.yAxis.parent = box;
    axes.zAxis.parent = box;
    let pressed = false;
    let vector = { x: '', y: '', z: '' };
    let pickedItem = undefined;
    scene.onPointerDown = function (event, pickResult) {
        console.log(pickResult);
        pressed = true;
        pickedItem = pickResult.pickedMesh;
        if (pickResult.pickedMesh && pickResult.pickedMesh.id !='box') {
            camera.attachControl(canvas, true);
        } else if (pickResult.pickedMesh && pickResult.pickedMesh.id == 'box') {
            camera.detachControl();
        }

        // pickResult.pickedMesh.material = material

        //left mouse click
        if (event.button == 0) {
            
            vector = pickResult.pickedPoint;
            if (vector) {
                // camera.detachControl();
                console.log('left mouse click: ' + vector.x + ',' + vector.y + ',' + vector.z);
                // const box = BABYLON.MeshBuilder.CreateBox("box", {height : vector.z});
                box.position.x = vector.x;
                box.position.y = vector.y;
                box.position.z = vector.z;

                var mesh = BABYLON.Mesh.MergeMeshes([pickedItem, box], true, true, undefined, false, true);
            }
        }
        //right mouse click
        if (event.button == 2) {
            vector.x = pickResult.pickedPoint.x;
            vector.y = pickResult.pickedPoint.y;
            vector.z = pickResult.pickedPoint.z;
            console.log('right mouse click: ' + vector.x + ',' + vector.y + ',' + vector.z);
        }
        //Wheel button or middle button on mouse click
        if (event.button == 1) {
            vector['x'] = pickResult.pickedPoint['x'];
            vector['y'] = pickResult.pickedPoint['y'];
            vector['z'] = pickResult.pickedPoint['z'];
            console.log('middle mouse click: ' + vector.x + ',' + vector.y + ',' + vector.z);
        }
    }

    ground.onPointerDown = function(){
        console.log("box pointer down");
        pickedItem = undefined;
    }

    scene.onPointerUp = function (event, pickResult){
        camera.attachControl(canvas, true);
        // pickResult.pickedMesh.material = "";
        pressed = false;

    }

    scene.onPointerMove = function(event, pickResult){
        if (pressed) {
            // console.log(pickResult);

            if (pickedItem && pickedItem.id == 'box') {
                const x = pickResult.ray.origin._x
                // const y = pickResult.ray.origin._y + pickResult.ray.direction._y
                const z = pickResult.ray.origin._z

                pickedItem.position.x = x; // red
                // pickedItem.position.y = y; // green
                pickedItem.position.z = z; // blue
            }

        }
    }

    scene.onKeyboardObservable.add((kbInfo) => {
        switch (kbInfo.type) {
            case BABYLON.KeyboardEventTypes.KEYDOWN:
                switch (kbInfo.event.key) {
                    case "a":
                    case "A":
                        box.position.x -= 0.1;
                        break
                    case "d":
                    case "D":
                        box.position.x += 0.1;
                        break
                    case "w":
                    case "W":
                        box.position.y += 0.1;
                        break
                    case "s":
                    case "S":
                        box.position.y -= 0.1;
                        break
                    case "q":
                    case "Q":
                        box.position.z -= 0.1;
                        break
                    case "e":
                    case "E":
                        box.position.z += 0.1;
                        break
                }
                break;
        }
    });


    // scene.debugLayer.show();
    return scene;
}
window.initFunction = async function () {


    var asyncEngineCreation = async function () {
        try {
            return createDefaultEngine();
        } catch (e) {
            console.log("the available createEngine function failed. Creating the default engine instead");
            return createDefaultEngine();
        }
    }

    window.engine = await asyncEngineCreation();
    if (!engine) throw 'engine should not be null.';
    startRenderLoop(engine, canvas);
    window.scene = await createScene();

};
initFunction().then(() => {
    sceneToRender = scene
});

// Resize
window.addEventListener("resize", function () {
    engine.resize();
});