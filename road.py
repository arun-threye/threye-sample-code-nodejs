import bpy
import os
 
# Append Asset files to content 
file_path = 'D:\\threye\BlenderAssets\Road 7.5m Profiled 26_8.blend'
inner_path = 'Object'
object_name = '7.5m Road'
output_dir = "D:\\threye\\output"
osm_file = "D:\\threye\\map.osm"
bpy.ops.importgis.osm_file("EXEC_DEFAULT", filepath=osm_file)

bpy.ops.wm.append(
    filepath=os.path.join(file_path, inner_path, object_name),
    directory=os.path.join(file_path, inner_path),
    filename=object_name
    )



context = bpy.context
obj = bpy.data.objects['Ways']
bpy.context.view_layer.objects.active = obj
obj.select_set(True)
#print(bpy.context.object.modifiers.active)

origRoad = bpy.data.objects['7.5m Road']
for modifier in origRoad.modifiers:
    print(modifier.name)
    print(bpy.ops.object)
    mSrc = modifier
    
    mDst = obj.modifiers.new(mSrc.name, mSrc.type)
    
    properties = [p.identifier for p in mSrc.bl_rna.properties
                          if not p.is_readonly]

            # copy those properties
    for prop in properties:
        setattr(mDst, prop, getattr(mSrc, prop))

    
    bpy.ops.object.modifier_apply(
        modifier=modifier.name
    )


bpy.ops.exportgis.shapefile(filepath=os.path.join(output_dir, "map.shp"))
