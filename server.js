const express = require('express')
var bodyParser = require('body-parser')
const axios = require('axios')
const fs = require('fs')
const path = require('path')
var cors = require('cors')


const app = express()
const port = 3234
// create application/json parser
// var jsonParser = bodyParser.json()
// app.use(bodyParser.json({ type: 'application/*+json' }))
app.use(cors())
app.use(express.raw())

const accessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJiOGRhZDQ2YS1jMzcyLTQ2ZTUtOWYyYy0wY2ZlYzE5ZjFlOGIiLCJpZCI6MTA1NTkxLCJhc3NldHMiOnsiMzk1NCI6eyJ0eXBlIjoiSU1BR0VSWSJ9fSwic3JjIjoiOWY2NDNkNGQtOGRlNy00MGQ3LWEzYjctZTAyOTliMWQ0MTliIiwiaWF0IjoxNjYzNzUxNDg0LCJleHAiOjE2NjM3NTUwODR9.DhfZjuCTuO3kO760UsaXcTbzkOEPUDk2P233_gI1lfg";

// Below is cesium Ion token
const accessToken_ion = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI5ZjY0M2Q0ZC04ZGU3LTQwZDctYTNiNy1lMDI5OWIxZDQxOWIiLCJpZCI6MTA1NTkxLCJpYXQiOjE2NjEyMjY1MDN9.II69wgF-qRcOU3tKXfRRzdW7_5o6mDyVTt1qB2FbWKY";

// const assetId = 69380; //69380
const assetId = 3954; //69380
// const assetId = 1; //69380

app.get('/*', async (req, res) => {
//   res.send('Hello World!')

    const path2 = path.resolve(__dirname, 'images', 'code.jpg')
    const writer = fs.createWriteStream(path2)


    console.log(req.originalUrl);
    if (req.originalUrl.includes("favicon.ico")) {
        res.send("");
        return;
    }
    var apiCall = async () => {
        try {


            // return await axios.get(`https://assets.cesium.com/${assetId}${req.originalUrl}`, {
            //     headers: { 'Authorization': `bearer ${accessToken}` }
            // })
            // return await axios.get(`https://assets.cesium.com/3954/4/16/8.jpg`, {
            //     responseType: 'stream',
            //     headers: { 'Authorization': `bearer ${accessToken}` }
            // })
            return await axios.get(`https://api.cesium.com/v1/assets/${assetId}/endpoint`, {
                headers: { 'Authorization': `bearer ${accessToken_ion}` }
            })
        } catch (error) {
            console.error(error)
        }
    }
    var resp = await apiCall()
    if (resp) {
        // if (req.originalUrl.includes("b3dm")) {
        //     // fs.writeFileSync(`./${req.originalUrl.split("?")[0]}`, resp.data)
        //     await res.pipe(resp.data);
        //     await res.on('end', function () {
        //         //res.end({"status":"Completed"});
        //     });
        // } else {
        //     res.send(resp.data)
        // }
        // res.setHeader('Content-Type', 'application/octet-stream');
        // resp.data.pipe(writer)
        // console.log(resp);
        // res.status(200);
        // // res.sendFile(filepath);
        // return new Promise((resolve, reject) => {
        //     writer.on('finish', resolve)
        //     writer.on('error', reject)
        // })
        res.send(resp.data)
    }


    // console.log(resp);
    // if (req.originalUrl.includes("json")) {
    //     res.send(resp.data)
    // } else {
    //     res.send(resp)
    // }

})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})